//-- HELP: https://github.com/twitter/typeahead.js/blob/master/doc/bloodhound.md
//-- HELP: https://github.com/twitter/typeahead.js/blob/master/doc/jquery_typeahead.md

$(document).ready(function() {
// constructs the suggestion engine
    var tags1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('tag'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10,
        prefetch: {
            url: '/api/1/suggestion',
            // cache time  (in milliseconds).  Defaults to 86400 000 (1 day).
            ttl: 120000,
            // the json file contains an array of strings, but the Bloodhound
            // suggestion engine expects JavaScript objects so this converts all of
            // those strings
            //            filter: function (list) {
            //                return $.map(list, function (obj) {
            //                    return obj;
            //                });
            //            }
        }
    });

    // kicks off the loading/processing of `local` and `prefetch`
    tags1.initialize();

    $('#prefetch .typeahead').typeahead(
    	// options
		{
			minLength: 1,
			highlight: true,
		},
		// datasets
		{
			name: 'tags1',
			displayKey: 'tag',
			// `ttAdapter` wraps the suggestion engine in an adapter that
			// is compatible with the typeahead jQuery plugin
			source: tags1.ttAdapter(),
			templates: {
                empty: [
                  '<div class="empty-message">',
                  'unable to find any Best Picture winners that match the current query',
                  '</div>'
                ].join('\n'),

                suggestion: Handlebars.compile('<p><strong>{{eng}}</strong> – {{kor}}</p>')
            }
		}
	);



    $('#prefetch .typeahead').bind("enterKey",function(e){
        if ($(this).val() == '') {
            return false;
        }
        $('#search').submit();
    });
    $('#prefetch .typeahead').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(this).trigger("enterKey");
        }
    });
});