# -*- coding: utf-8 -*-

from google.appengine.ext import blobstore, ndb
from google.appengine.ext.webapp import blobstore_handlers
from webapp2 import uri_for

from . import ViewHandler, is_none_or_empty
from ..models import Content, Tag
import util


class ListView(ViewHandler):
    def get(self):
        all = Content.query().fetch()
        self.add_targ('contents', all)
        return self.response_html('admin/list.html')


class SearchView(ViewHandler):
    def get(self):
        query = self.request.get('query', '')
        query = query.encode('utf8').strip()

        # tag_key = Tag.query(Tag.tags == query).get().key

        contents = Content.query(Content.tags == query).fetch()
        contents_dict = contents
        if len(contents_dict) == 1:
            code = contents_dict[0].get_code()
            return self.redirect_to('content', content_code=code)

        self.add_targ('contents', contents_dict)
        return self.response_html('admin/list.html')


class ModifyView(ViewHandler, blobstore_handlers.BlobstoreUploadHandler):
    def get(self):
        content_code = self.request.get('content_code', '')

        content = Content.get_by_code(content_code)

        if not content:
            return self.redirect_to('admin-index')

        upload_url = blobstore.create_upload_url(uri_for('admin-modify'))
        self.add_targ('upload_url', upload_url)
        self.add_targ('content', content)
        return self.response_html('admin/modify.html')

    def post(self):
        title = self.request.get('title')
        tags = self.request.get('tags')
        body = self.request.get('body')
        content_code = self.request.get('content_code')

        if is_none_or_empty(content_code) or is_none_or_empty(title) or is_none_or_empty(tags) or is_none_or_empty(body):
            #Todo: error
            return self.redirect_to('admin-index')

        content = Content.get_by_code(content_code)

        if not content:
            return self.redirect_to('admin-index')


        movie_url = self.request.get('movie_url')

        image_info = None
        upload_files = self.get_uploads('file')
        if upload_files:
            image_info = upload_files[0]

        image_key = image_info.key() if image_info else None


        content.title = title
        content.tags = tags
        body = body.replace('\r\n', '\n')
        content.body = body
        content.movie_url = movie_url
        content.image_info = image_key

        content.put()

        return self.redirect_to('admin-index')


class PublishView(ViewHandler):
    def post(self):
        content_code = self.request.get('content_code')
        if is_none_or_empty(content_code):
            #Todo: error
            return self.redirect_to('admin-index')
        content = Content.get_by_code(content_code)

        content.is_published = True
        content.put()
        return self.redirect_to_referer()


class UnpublishView(ViewHandler):
    def post(self):
        content_code = self.request.get('content_code')
        if is_none_or_empty(content_code):
            #Todo: error
            return self.redirect_to('admin-index')
        content = Content.get_by_code(content_code)

        content.is_published = False
        content.put()
        return self.redirect_to_referer()