# -*- coding: utf-8 -*-

from google.appengine.ext import blobstore, ndb
from google.appengine.ext.webapp import blobstore_handlers
from webapp2 import uri_for

from . import ViewHandler, is_none_or_empty
from ..models import *
import util

def have_writer(super):
    def _have_writer(func):
        def _inner_func(self, *args, **kwargs):
            return func(self, *args, **kwargs)
            if self.user and self.user.write_perm:
                return func(self, *args, **kwargs)
            return self.response_error("콘텐트 작성 권한이 없습니다")
        return _inner_func

    if hasattr(super, 'get'):
        super.get = _have_writer(super.get)
    if hasattr(super, 'post'):
        super.post = _have_writer(super.post)
    return super


@have_writer
class ListView(ViewHandler):
    def get(self):
        all = Content.query(Content.owner == self.user.key).fetch()
        self.add_targ('contents', all)
        self.add_targ('user', self.user)
        return self.response_html('cms/list.html')

@have_writer
class SearchView(ViewHandler):
    def get(self):
        query = self.request.get('query', '')
        query = query.encode('utf8').strip()

        contents = Content.query(Content.tags == query).fetch()
        contents_dict = contents
        if len(contents_dict) == 1:
            code = contents_dict[0].get_code()
            return self.redirect_to('content', content_code=code)

        self.add_targ('contents', contents_dict)
        self.add_targ('user', self.user)
        return self.response_html('cms/list.html')

@have_writer
class WriteView(ViewHandler, blobstore_handlers.BlobstoreUploadHandler):
    def get(self):
        upload_url = blobstore.create_upload_url(uri_for('cms-write'))
        self.add_targ('upload_url', upload_url)
        return self.response_html('cms/write.html')

    def post(self):
        title = self.request.get('title')
        tags = self.request.get('tags')
        body = self.request.get('body')

        movie_url = self.request.get('movie_url')

        if is_none_or_empty(title) or is_none_or_empty(tags) or is_none_or_empty(body):
            #Todo: error
            return self.redirect_to('cms-index')

        image_info = None
        upload_files = self.get_uploads('file')
        if upload_files:
            image_info = upload_files[0]

        image_key = image_info.key() if image_info else None

        body = body.replace('\r\n', '\n')
        content = Content(title=title, tags=tags, body=body, image_info=image_key, movie_url=movie_url, owner=self.user.key)
        c_key = content.put()

        new_obj = EngKorPair(parent=c_key, eng=title, kor=title, tags=tags)
        new_key = new_obj.put()

        return self.redirect_to('cms-index')

@have_writer
class ModifyView(ViewHandler, blobstore_handlers.BlobstoreUploadHandler):
    def get(self):
        content_code = self.request.get('content_code', '')

        content = Content.get_by_code(content_code)

        if not content:
            return self.redirect_to('cms-index')

        upload_url = blobstore.create_upload_url(uri_for('cms-modify'))
        self.add_targ('upload_url', upload_url)
        self.add_targ('content', content)
        self.add_targ('user', self.user)
        return self.response_html('cms/modify.html')

    def post(self):
        title = self.request.get('title')
        tags = self.request.get('tags')
        body = self.request.get('body')
        content_code = self.request.get('content_code')

        if is_none_or_empty(content_code) or is_none_or_empty(title) or is_none_or_empty(tags) or is_none_or_empty(body):
            #Todo: error
            return self.redirect_to('cms-index')

        content = Content.get_by_code(content_code)

        if not content:
            return self.redirect_to('cms-index')


        movie_url = self.request.get('movie_url')

        image_info = None
        upload_files = self.get_uploads('file')
        if upload_files:
            image_info = upload_files[0]

        image_key = image_info.key() if image_info else None


        content.title = title
        body = body.replace('\r\n', '\n')
        content.body = body
        content.movie_url = movie_url
        content.image_info = image_key

        content.put()

        return self.redirect_to('cms-index')

@have_writer
class PublishView(ViewHandler):
    def post(self):
        content_code = self.request.get('content_code')
        if is_none_or_empty(content_code):
            #Todo: error
            return self.redirect_to('cms-index')
        content = Content.get_by_code(content_code)

        content.is_published = True
        content.put()
        return self.redirect_to_referer()

@have_writer
class UnpublishView(ViewHandler):
    def post(self):
        content_code = self.request.get('content_code')
        if is_none_or_empty(content_code):
            #Todo: error
            return self.redirect_to('cms-index')
        content = Content.get_by_code(content_code)

        content.is_published = False
        content.put()
        return self.redirect_to_referer()