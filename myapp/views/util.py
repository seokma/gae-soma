# coding: utf-8
__author__ = 'pedium'

import urllib
import re

re_youtubecom = re.compile(r'youtube.com/watch\?v=(.+)')
re_youtube  = re.compile(r'youtu.be/(.+)')

"""
URL 에서 youtube 동영상의 식별자 코드(ccode, content code)를 구한다.
"""
def get_youtube_ccode(url):
    '''
    INPUT
    http://www.youtube.com/watch?v=stKAkajlaJ8
    http://youtu.be/stKAkajlaJ8

    GOOD OUTPUT
    stKAkajlaJ8	(== ccode == content code)

    NO OUTPUT
    return '' if not found
    '''

    p = re_youtubecom.search(url)
    if p:
        ccode = p.group(1)
        if len(ccode)==11:
            return ccode

    p = re_youtube.search(url)
    if p:
        ccode = p.group(1)
        if len(ccode)==11:
            return ccode

    return ''


YOUTUBE_IFRAME_URL = "http://www.youtube.com/embed/{0}?rel=0&amp;showinfo=0"


def refine_to_iframe_url_of_youtube(youtube_url):
    code = get_youtube_ccode(youtube_url)
    if code and code != '':
        return YOUTUBE_IFRAME_URL.format(code)
    return None

"""
convert the parameter part of a url into a dict.

INPUT: category=1&title=%EA%B0%80%EC%A0%95%EB%B2%95&body=%EA%B0%80%EC%A0%95%EB%B2%95&tags=%EA%B0%80%EC%A0%95%EB%B2%95
OUTPUT:
{'body': '%EA%B0%80%EC%A0%95%EB%B2%95', 'title': '%EA%B0%80%EC%A0%95%EB%B2%95', 'category': '1', 'tags': '%EA%B0%80%EC%A0%95%EB%B2%95'}
{'tags': u'\uac00\uc815\ubc95', 'category': u'1', 'title': u'\uac00\uc815\ubc95', 'body': u'\uac00\uc815\ubc95'}

"""
def convert_paras_to_dict(paras):
    #d = dict([ tuple(one.split('=')) for one in paras.split('&')])
    d = {}
    for one in paras.split('&'):
        k,v = tuple(one.split('='))
        v = urllib.unquote(v).decode('utf8')
        d[k] = v
    return d


"""
"""
def convert_boolean_in_dict(d):
    for k,v in d.iteritems():
        if type(v) in [str, unicode]:
            if v.upper()=='TRUE': d[k] = True
            elif v.upper()=='FALSE': d[k] = False



"""
"""
def parse_tags(ts):
    tags = map(lambda x: x.strip(), ts.split(','))
    return tags

"""
같은 email 주소에 대해서도 key를  생성할 때 마다 key 값은 달라져야 한다.

gen_confirm_key: hashkey 리턴
encode_passwd(): hashkey, salt 둘다 리턴
"""
import hashlib
from random import random

def gen_confirm_key(emailstr):
    salt = hashlib.md5(str(random())).hexdigest()[:5]
    src = salt + emailstr
    hashkey = hashlib.sha256(src).hexdigest()
    return hashkey

def encode_passwd(passwd):
    salt = hashlib.md5(str(random())).hexdigest()[:5]
    src = salt + passwd
    hashkey = hashlib.sha256(src).hexdigest()
    return hashkey, salt

def check_passwd(passwd, salt, passwd_encoded):
    src = salt + passwd
    hashkey = hashlib.sha256(src).hexdigest()
    return hashkey == passwd_encoded

if __name__ == '__main__':
    tests = ['http://www.youtube.com/watch?v=stKAkajlaJ8', 'https://youtu.be/stKAkajlaJ8', 'ttp://youtu.be/stKAkajlaJ8', 'youtu.be/stKAkajlaJ8', 'stKAkajlaJ8' ]
    for t in tests:
        print t, get_youtube_ccode(t)

    print gen_confirm_key('handol@gmail.com')
    print encode_passwd('handol@gmail.com')