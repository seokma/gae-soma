# -*- coding: utf-8 -*-

from webapp2 import Route
from webapp2_extras import routes

from ..views.myapp import *
from ..views import cms, admin
from ..apis.myapp import *
urls = [
    Route('/', IndexView, name="index"),
    Route('/signin', SigninView, name="signin"),
    Route('/signup', SignupView, name="signup"),
    Route('/signout', SignoutView, name="signout"),
    Route('/passwd', PasswdView, name="passwd"),
	Route('/confirm', ConfirmView, name="confirm"),

    Route('/search', SearchView, name="search"),
    Route('/c/<content_code>', ContentView, name="content"),


    routes.PathPrefixRoute('/cms', [
        Route('/', cms.ListView, name="cms-index"),
        Route('/write', cms.WriteView, name="cms-write"),
        Route('/list', cms.ListView, name="cms-list"),
        Route('/search', cms.SearchView, name="cms-search"),
        Route('/modify', cms.ModifyView, name="cms-modify"),
        Route('/publish', cms.PublishView, name="cms-publish"),
        Route('/unpublish', cms.UnpublishView, name="cms-unpublish")
    ]),
    routes.RedirectRoute('/cms', redirect_to_name="cms-index"),


    routes.PathPrefixRoute('/admin', [
        Route('/', admin.ListView, name="admin-index"),
        Route('/list', admin.ListView, name="admin-list"),
        Route('/search', admin.SearchView, name="admin-search"),
        Route('/modify', admin.ModifyView, name="admin-modify"),
        Route('/publish', admin.PublishView, name="admin-publish"),
        Route('/unpublish', admin.UnpublishView, name="admin-publish"),
    ]),
    routes.RedirectRoute('/admin', redirect_to_name="admin-index"),

    routes.PathPrefixRoute('/api' + '/1', [
        Route('/suggestion', SuggestionApi),
        Route('/storedata', StoreDataApi),
        Route('/storecontent', StoreContentApi),
        Route('/storepair', StorePairApi),
        Route('/content/<content_code>/publish', PublishApi, name="api-publish"),
        Route('/content/<content_code>/unpublish', UnpublishApi, name="api-unpublish")

    ])
]