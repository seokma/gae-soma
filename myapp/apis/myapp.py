# -*- coding: utf-8 -*-


import webapp2
from webapp2_extras import sessions
import json

from ..models import *
from  ..views import util

import logging

class ApiHandler(webapp2.RequestHandler):
    def response_to_json(self, obj):
        return self.response.write(json.dumps(obj))

class AuthorizedApiHandler(ApiHandler):
    @webapp2.cached_property
    def session(self):
        """Returns a session using the default cookie key"""
        self.session_store = sessions.get_store(request=self.request)
        return self.session_store.get_session()

    @webapp2.cached_property
    def user(self):
        user_id = self.session.get('user_id')
        if user_id:
            return Account.get_by_id(user_id)


class SuggestionApi(ApiHandler):
    def get(self):
        #t = Tag.query().fetch()
        #keyword = map(lambda x: x.to_dict(), t)
        res = []
        pairs = EngKorPair.query().fetch()
        for p in pairs:
            for tag in p.tags:
                d = {'tag':tag, 'eng': p.eng, 'kor': p.kor}
                res.append(d)

        return self.response_to_json(res)


"""
Table 이름을 지정하여 데이타를 저장할 수 있다.
"""
class StoreDataApi(ApiHandler):
    def post(self):
        paras = self.request.body
        jsondata = json.loads(paras, encoding='utf-8')
        #jsondata = util.convert_paras_to_dict(paras)
        util.convert_boolean_in_dict(jsondata)
        #print jsondata
        table = jsondata.get('table', 'Content') # if table name is not given, it's 'Content' table

        new_obj = eval("%s(**jsondata)" % table)
        new_key = new_obj.put()
        if new_key:
            return self.response_to_json({'ok': 1})
        else:
            return self.response_to_json({'ok': 0})

"""
Content 데이타 저장
"""
class StoreContentApi(ApiHandler):
    def post(self):
        paras = self.request.body
        jsondata = json.loads(paras, encoding='utf-8')
        #jsondata = util.convert_paras_to_dict(paras)
        util.convert_boolean_in_dict(jsondata)
        logging.info(jsondata)
        new_obj = Content(**jsondata)
        new_key = new_obj.put()

        new_obj = EngKorPair(**jsondata)
        new_key = new_obj.put()

        if new_key:
            return self.response_to_json({'ok': 1})
        else:
            return self.response_to_json({'ok': 0})

"""
EngKorPair 데이타 저장
"""
class StorePairApi(ApiHandler):
    def post(self):
        paras = self.request.body
        jsondata = json.loads(paras, encoding='utf-8')
        #jsondata = util.convert_paras_to_dict(paras)
        util.convert_boolean_in_dict(jsondata)
        logging.info(jsondata)
        new_obj = EngKorPair(**jsondata)
        new_key = new_obj.put()
        if new_key:
            return self.response_to_json({'ok': 1})
        else:
            return self.response_to_json({'ok': 0})


class PublishApi(AuthorizedApiHandler):
    def post(self, content_code):
        if not self.user or not self.user.publish_perm:
            return self.response_to_json({'ok': 0})

        content = Content.get_by_code(content_code)

        if not content:
            return self.response_to_json({'ok': 0})
        content.is_published = True
        content.put()
        return self.response_to_json({'ok': 1})

class UnpublishApi(AuthorizedApiHandler):
    def post(self, content_code):
        if not self.user or not self.user.publish_perm:
            return self.response_to_json({'ok': 0})
        
        content = Content.get_by_code(content_code)

        if not content:
            return self.response_to_json({'ok': 0})
        content.is_published = False
        content.put()
        return self.response_to_json({'ok': 1})