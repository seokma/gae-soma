# -*- coding: utf-8 -*-

import webapp2
from routes import urls

from .config.env import APP_ENV
from .config.webapp2 import app_config


if APP_ENV == 'development':
    application = webapp2.WSGIApplication(urls, config=app_config, debug=True)
elif APP_ENV == 'production':
    application = webapp2.WSGIApplication(urls, config=app_config)


if __name__ == "__main__":
    pass
