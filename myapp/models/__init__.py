# -*- coding: utf-8 -*-

from google.appengine.ext import ndb as models
import logging

class BaseModel(models.Model):
    def __init__(self, **kwargs):
        models.Model.__init__(self)
        for k,v in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, v)

    @classmethod
    def get_by_key_id(cls, _id):
        if _id is None or _id == '':
            return None

        if isinstance(_id, str) or isinstance(_id, unicode):
            _id = int(_id)

        if _id == 0:
            return None

        k = models.Key(cls.__name__, _id)
        return k.get()

    @classmethod
    def id_to_code(cls, _id):
        h = hex(_id)
        if h[-1]=='L':
            h = h[:-1]
        h_nozero = h.rstrip('0')
        n = len(h) - len(h_nozero)
        code = h_nozero[2:] + '%02x' % (n)
        return code

    @classmethod
    def code_to_id(cls, _code):
        n_zero = int(_code[-2:], base=16)
        if n_zero >= 16: # id is 64-bit value. so the n_zero cannot be 16 digit long in hex.
            return 0
        id = int(_code[:-2], base=16)
        id = id << (n_zero * 4)
        return id

    @classmethod
    def get_by_code(cls, code):
        id = cls.code_to_id(code)
        return cls.get_by_key_id(id)

    '''
    set the value of an object from a dictionary
    '''
    """
    def from_dict(self, D):
        for name, val in D.iteritems():
            if hasattr(self, name):
                setattr(self, name, val)
        return self
    """

    def to_dict(self, *args, **kawrs):
        ret = models.Model._to_dict(self, *args, **kawrs)
        ret['id'] = self.key.id()
        ret['code'] = BaseModel.id_to_code(self.key.id())
        return ret

    def get_code(self):
        return BaseModel.id_to_code(self.key.id())


class Account(BaseModel):
    email = models.StringProperty(indexed=True)
    write_perm = models.BooleanProperty(default=False, indexed=False)
    publish_perm = models.BooleanProperty(default=False, indexed=False)
    email_confirmed = models.BooleanProperty(default=False, indexed=False)
    passwd = models.StringProperty(indexed=False)
    passwd_salt = models.StringProperty(indexed=False)
    confirm_key = models.StringProperty(indexed=True) # email confirm key
    reset_key = models.StringProperty(indexed=True) # passwd reset key
    keygen_time = models.DateTimeProperty(indexed=False) # confirm key generation time. expire 여부 검사하기 위해.
    confirm_time = models.DateTimeProperty(indexed=False) # user의 email confirm time
    signup_time = models.DateTimeProperty(auto_now_add=True, indexed=True) # signup time
    last_time = models.DateTimeProperty(auto_now=True, indexed=True) # the last time of login


class Category(BaseModel):
    category = models.StringProperty()
    description = models.StringProperty(indexed=False)


class Tag(BaseModel):
    tag = models.StringProperty()

    @classmethod
    def add_tag(cls, t):
        r = cls.query(cls.tag == t).get()
        if r is None:
            r = cls(tag=t)
            r.put()
            logging.info('TAG INSERT: %s'% t)
        return r



class Content(BaseModel):
    category = models.StringProperty()
    title = models.StringProperty()
    body = models.TextProperty(indexed=False)
    tags = models.StringProperty(repeated=True)
    image_info = models.BlobKeyProperty(indexed=False)
    movie_url = models.StringProperty(indexed=False)
    movie_code = models.StringProperty(indexed=False)
    rel_url = models.StringProperty(indexed=False) # related URL of the content
    is_published = models.BooleanProperty(default=False, indexed=False)
    owner = models.KeyProperty(kind='Account')

    def __init__(self, **kwargs):
        models.Model.__init__(self)
        for k,v in kwargs.iteritems():
            ''' tags 는 comma-separated 문자열. tag의 목록으로 분리함. '''
            if k=='tags' and type(v) is not list:
                v = [t.strip() for t in v.split(',')]
            if hasattr(self, k):
                setattr(self, k, v)

    def _pre_put_hook(self):
        ''' store each tag into Tag table '''
        for t in self.tags:
            Tag.add_tag(t)


class EngKorTag(Tag):
    tag = models.StringProperty()


class EngKorPair(BaseModel):
    parent = models.KeyProperty(kind='Content')
    tags = models.StringProperty(repeated=True)
    eng = models.StringProperty(indexed=False)
    kor = models.StringProperty(indexed=False)

    def __init__(self, **kwargs):
        models.Model.__init__(self)
        for k,v in kwargs.iteritems():
            ''' tags 는 comma-separated 문자열. tag의 목록으로 분리함. '''
            if k=='tags' and type(v) is not list:
                v = [t.strip() for t in v.split(',')]
            if hasattr(self, k):
                setattr(self, k, v)

    def _pre_put_hook(self):
        ''' store each tag into Tag table '''
        for t in self.tags:
            EngKorTag.add_tag(t)


